FROM busybox:latest

RUN echo "Do your build here"; \
echo "one" >> house.txt; \
echo "two" >> house.txt;\
echo "<!DOCTYPE html><html><body><h1>NOT FOUND!!!</h1></body></html>" >> 404.txt

RUN mkdir public &&\
cp house.txt public/index.html &&\
cp 404.txt public/404.html

